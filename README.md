
<div align="right">
  <img src="https://eneskzlcn.github.io/my-published-images/todo-icon.png" width="50" height="50">
</div>
<h3 align="center">About Todo Frontend Project</h3>
<br>

<hr>

#### Project Information And Purpose

This project is a frontend part of the todo assignment project which provides a user interface to add new todos and list todos. User interface design technologies used in project are, nuxtjs, vuejs and also tailwindcss as style.

Todo assignment project developed with **ATDD** approach which stands for **'Acceptance Test Driven Development'**. Acceptance test is in different <a href="https://gitlab.com/todo32/acceptance">repository</a> and getting written synchronously with this project for each specific scenario.

As the ATDD approach, a scenario that written in acceptance part is implemented here with writing related component and unit tests first. After the tests written for the expectations, the actual components and functionalities is written. After all user acceptance scenarios, unit and component tests passes, consumer test is written according to expectation from backend api.

As a result of consumer test, pacts are generated and then as last step for this project, pacts are published to the [pactflow.io](https://pactflow.io/).

<hr>

#### Developing a Single Scenario with TDD Aproach

First of all, suppose that we have scenario for adding todo coming from acceptance side. First thing that you should do to think about how to actualize `AddTodo` scenario and what components and units that you need. For this scenario you need a button and input as form component and API as a unit. After all this clarifications made for this scenario you can start your first component test which is `TodoForm` here.
```js
// TodoForm.spec.js
import { mount } from '@vue/test-utils'
import TodoForm from '~/components/TodoForm.vue'

describe('TodoForm', () => {
  test('renders correctly', () => {
    const wrapper = mount(TodoForm)
    expect(wrapper.vm).toBeTruthy()
  })
  test('todo input renders correctly', () => {
    const wrapper = mount(TodoForm)
    const todoInput = wrapper.find('#todo-input')
    expect(todoInput.attributes().placeholder).toEqual('Enter a todo...')
  })
  test('todo input connected to component data model successfully', () => {
    const todoText = 'buy some milk'
    const wrapper = mount(TodoForm)
    const todoInput = wrapper.find('#todo-input')
    todoInput.setValue(todoText)
    expect(wrapper.vm.$data.todoInp).toEqual(todoText)
  })
  test('calls save function on click to save button', async () => {
    const wrapper = mount(TodoForm)
    wrapper.setMethods({
      saveTodo: jest.fn()
    })
    await wrapper.find('#save-button').trigger('click')
    expect(wrapper.vm.saveTodo).toHaveBeenCalled()
  })

})
```
You need to expect that code has to be failed. Because there is no component yet. Now you can create a component.
```html
<!-- TodoForm.vue -->
<template>
  <div class="mt-10 border rounded-xl shadow-2xl">
    <input id="todo-input" v-model="todoInp" type="text" placeholder="Enter a todo..." class="border h-8 mr-2 w-80 shadow-2xl">
    <button id="save-button" class="border rounded-xl w-28 h-8 shadow-2xl" @click="saveTodo">
      Save
    </button>
  </div>
</template>

<script>
import Vue from 'vue'

export default Vue.extend({
  name: 'TodoForm',
  data () {
    return {
      todoInp: ''
    }
  },
  methods: {
    saveTodo () {
      // TODO
    }
  }
})
</script>
```
Now you can expect to pass your test. The next step is making a POST call to backend service. Before the making a API call you have to write your unit test. As an architecture decision you can write an API class to handle your requests.
Create a folder and put the easiest api class there.

```js
// api/api.js

import axios from 'axios'

export class API {
  constructor (URL) {
    this.URL = URL
    axios.defaults.baseURL = URL
  }
  async addTodo(todo) {
    //TODO
  }
}
```
After that, you can write your unit test as shown below for post scenario.
```js
// test/unit/api.spec.js

import axios from 'axios'
import { API } from '~/api/api'

jest.mock('axios') // already

describe('Api', () => {
  test('initializes successfully with given url', () => {
    const testURL = 'http://testing-url'
    const api = new API(testURL)
    expect(api.URL).toEqual(testURL)
  })
  describe('postTodo function of API class returns', () => {
    test('added todo if valid request sends', async () => {
      const testMockData = {
        task: 'buy some milk'
      }
      const expectedResponse = {
        id: 1,
        task: 'bu some milk'
      }
      axios.post.mockResolvedValue({
        data: expectedResponse
      })
      const addedTodo = await api.addTodo(testMockData)
      expect(addedTodo).toEqual(expectedResponse)
    })
  })
})
```
You expect this test to fail because of the addTodo function is empty. So refactor your API class with adding following code
```js
  async addTodo (todo) {
    return await axios.post(`${this.URL}/todos`, todo).then(r => r.data)
}
```
After you add this to api, you can expect the test is pass. For last step of this scenario you can add a consumer contract test
to make sure you have designed your API class according to the expectations from backend correctly.
```js
  //test/cdc/Consumer.spec.js

  import { pactWith } from 'jest-pact'
  import { Matchers } from '@pact-foundation/pact'
  import { API } from '~/api/api.js'
  const { like } = Matchers
  pactWith({ consumer: 'todo-frontend', provider: 'todo-backend', pactfileWriteMode: 'overwrite', cors: true, spec: 2 }, (provider) => {
    describe('Todos', () => {
      let client
      beforeEach(() => {
        client = new API(provider.mockService.baseUrl)
      })
      test('Post Todo', async () => {
        const postTodoRequest = {
          task: 'get some bread'
        }
        await provider.addInteraction({
          state: 'posted todo successfully',
          uponReceiving: 'POST todos request',
          withRequest: {
            path: '/todos',
            method: 'POST',
            body: like(postTodoRequest)
          },
          willRespondWith: {
            headers: { 'Content-Type': 'application/json' },
            status: 200,
            body: like(todoRequest)
          }
        })
        await client.addTodo(postTodoRequest)
      })
    })
  })
```
After that, you expect your test to pass if everything is okay with API.
Congrats! You have completed an ATDD cycle for a scenario completely 
from acceptance test to the consumer test. All you need to do next is,
continue with developing backend application with TDD and BFF approach.

<hr>

#### CI-CD Pipeline Of Project

The CI-CD pipeline has the following stages:

- build
- test
- package-test
- prepare-test-artifacts
- deploy-to-test-env
- pact-consumer-test
- pact-publish-test
- acceptance-test
- package-prod
- prepare-prod-artifacts
- deploy-to-prod-env
- pact-publish-prod

**Build:** 
  In the build stage, the project is building with a node docker image and
we expect the project build successfully in this stage.

**test:**
  In the test stage, all unit and component tests of the project runs in a node
docker image. Expect the test pass to complete successfully this stage.

**package-test:** 
  In the package-test stage, the project is containerized with docker using a docker image and dind(docker in docker)
service. The reason for using the dind service is to make the docker image run in the runner docker image successfully.
Purpose of this stage is running the docker commands to build, tag and publish the project as docker image to the [docker.hub](https://hub.docker.com/)
The packaged project image is Dockerfile.dev which stands on project directory. This
dockerfile will be used in test environment.

**prepare-test-artifacts:**
  In the prepare-test-artifacts stage, helm charts of todo-frontend project
which in [todo-helm](https://gitlab.com/todo32/helm) repository downloaded and used to render the newest todo-frontend
deployment artifacts including the newest version of todo-frontend-test image which is built in package-test stage.
After all needed k8s deployment files for test environment is rendered with specific helm values, all rendered
deployment files are pushed to the [todo-deployment-artifacts](https://gitlab.com/todo32/deployment-artifacts) repository to use later to deploy.

**deploy-to-test-env:**
  In the deploy-to-test-env, all the newest deployment files just added to the deployment-artifacts repo in previous stage,
  is deployed to the test environment with argocd which already inside the test k8s cluster. For needed
  configs, gcloud cli is used.

**pact-consumer-test:**
  In this stage, the written consumer tests executed. Expect the consumer test passes all cases and 
  generates the pacts to publish in next stage.

**pact-publish-test:**
  In this stage, the generated pact file in previous stage published to the pactflow.io. Expect the pacts
  published successfully to the pactflow.io tagged with 'test' stands for test environment.

**acceptance-test:**
  This stage triggers the pipeline of [acceptance-test](https://gitlab.com/todo32/acceptance) project
which runs all acceptance test scenarios and expect all to pass.

**package-prod**
 In this stage, production image of the project containerized with docker with the Dockerfile which is in the
project root directory.

**prepare-prod-artifacts**
 This stage work exactly same with prepare-test-artifacts stage except the variables passed to the helm values.
 This stage prepared the deployment artifacts and replace the older ones which is in todo-deployment-artifacts repository with newest ones

**deploy-to-prod-env:**
  This stage exactly same with the deploy-to-test-env. Just the configs which is need to connect the cluster
are changes.

**pact-publish-prod:**
In this stage, the generated pact file in previous stage published to the pactflow.io. Expect the pacts
published successfully to the pactflow.io tagged with 'prod' stands for production environment.

<hr>

#### Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

```
#### Docker Setup For Development Serve

```bash
  #build image
  $ docker build -f Dockerfile.dev -t <image_name>:<tag_name> --build-arg \
   BACKEND_URL=<backend_url> <project_directory>

  # run image
  $ docker run <image_name>:<tag_name>
```
#### Docker Setup For Production

```bash
  #build image
  $ docker build -t <image_name>:<tag_name> --build-arg \
   BACKEND_URL=<backend_url> <project_directory>

  # run image
  $ docker run <image_name>:<tag_name>
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

#### Reachable Project Parts
  Todo assignment project consist of 5 different projects including this project. You can reach;
  - Todo project acceptance repository <a href="https://gitlab.com/todo32/acceptance"> here</a>,
  - Todo project backend repository <a href="https://gitlab.com/todo32/backend"> here</a>,
  - Todo project helm repository <a href="https://gitlab.com/todo32/helm"> here</a>,
  - Todo project deployment artifacts repository <a href="https://gitlab.com/todo32/deployment-artifacts"> here </a>

#### Running Project Environments

The all parts of project getting deployed into to environments; test and production environments. The project is live and available in;
- Test environment <a href="http://34.116.156.27:8090/">here</a>,
- Production environment <a href="http://34.116.223.97:8090/">here</a>.


#### References

- **Self Made**
  - Kubernetes [docs](https://eneskzlcn.github.io/my-documentations/CI-CD/Kubernetes/Index)
  - ArgoCD [docs](https://eneskzlcn.github.io/my-documentations/CI-CD/ArgoCD/Index)
  - Google Cloud K8s Engine [docs](https://eneskzlcn.github.io/my-documentations/CI-CD/GoogleK8S/Index)
- **External**
  - Kubernetes [docs](https://kubernetes.io/docs/home/), [playground](https://www.katacoda.com/courses/kubernetes/playground)
  - ArgoCD [docs](https://argo-cd.readthedocs.io/en/stable/)
  - Helm [docs](https://helm.sh/docs/)
  - Very beneficial video source [TechWorld With Nana](https://www.youtube.com/c/TechWorldwithNana) from youtube for all devops technologies including k8s, argocd, helm or etc.
  - Nuxtjs [docs](https://nuxtjs.org/docs/get-started/installation)
  - Vue3js [docs](https://vuejs.org/guide/introduction.html), vue test utils [docs](https://v1.test-utils.vuejs.org/)
  - Jest [docs](https://jestjs.io/docs/getting-started), [api](https://jestjs.io/docs/api)
<div align="right">
  <img src="https://nuxtjs.ir/logos/nuxt-icon-white.png" width="40" height="40">
  <img src="https://cdn3.iconfinder.com/data/icons/logos-and-brands-adobe/512/367_Vuejs-512.png" width="40" height="40">

nuxtjs | vuejs
</div>
<br>
